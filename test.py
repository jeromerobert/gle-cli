#! /usr/bin/env python

from subprocess import *
from array import *
from struct import *
from math import *
from vtk import *

def create_points(array):
    """Create vtkPoints from double array"""
    vtk_points = vtkPoints()
    double_array = vtkDoubleArray()
    double_array.SetVoidArray(array, len(array), 1)
    double_array.SetNumberOfComponents(3)
    vtk_points.SetData(double_array)
    return vtk_points

def create_cells(nb, array):
    """Create a vtkCellArray from long array"""
    vtk_cells = vtkCellArray()
    vtk_id_array = vtkIdTypeArray()
    vtk_id_array.SetVoidArray(array, len(array), 1)
    vtk_cells.SetCells(nb, vtk_id_array)
    return vtk_cells

def create_profile():
    a = array('d', [])
    n = 500
    r = 8.0
    for i in xrange(n):
        angle = 3*pi / n * i
        a.extend((r*cos(angle), r*sin(angle), float(50.0*i/n)))
    return a
#contour = array('d', [0,0, 0,1, 1,1, 0,0])
contour = array('d', [-0.2, -0.2, -0.2, 0.8, 0.8, -0.2, -0.2,-0.2])
points = create_profile()
points.extend(array('d', [0,1,-1, 0,0,0, 0,0,5, 0,5,5, 5,5,5, 6,5,5]))

"""
#define TUBE_JN_RAW          0x1
#define TUBE_JN_ANGLE        0x2
#define TUBE_JN_CUT          0x3
#define TUBE_JN_ROUND        0x4
#define TUBE_JN_MASK         0xf    /* mask bits */
#define TUBE_JN_CAP          0x10

/* Determine how normal vectors are to be handled */
#define TUBE_NORM_FACET      0x100
#define TUBE_NORM_EDGE       0x200
#define TUBE_NORM_PATH_EDGE  0x400 /* for spiral, lathe, helix primitives */
#define TUBE_NORM_MASK       0xf00    /* mask bits */
"""

ps = Popen(['./gle-cli', str(int("4", 16)), "1", "1", "0"], stdin = PIPE, stderr = PIPE)
ps.stdin.write(pack('i', len(contour)))
contour.tofile(ps.stdin)
ps.stdin.write(pack('i', len(points)))
points.tofile(ps.stdin)
ps.stdin.flush()

polydata = vtkPolyData()
points = array('d', [])
strip = array('l', [])
offset = 0
nb_strip = 0

while True:
    n = unpack('i', ps.stderr.read(4))[0]
    if n < 0:
        break
    points.read(ps.stderr, n)
    strip_size = n / 3
    strip.append(strip_size)
    strip.extend(offset + j for j in xrange(strip_size))
    offset += strip_size
    nb_strip += 1

polydata.SetPoints(create_points(points))
polydata.SetStrips(create_cells(nb_strip, strip))

tf = vtkTriangleFilter()
clean = vtkCleanPolyData()
clean.ToleranceIsAbsoluteOn()
clean.SetAbsoluteTolerance(1E-4)
clean.ConvertPolysToLinesOff()
tf.SetInput(polydata)
clean.SetInputConnection(tf.GetOutputPort())
writer = vtkXMLPolyDataWriter()
writer.SetInputConnection(clean.GetOutputPort())
writer.SetFileName("zob.vtp")
writer.SetDataModeToAscii()
writer.Write()

call(['paraview', 'zob.vtp'])
