#include <stdlib.h>
#include "vvector.h"
extern double gle_cli_matrix[4][4];
extern double * gle_cli_buffer;
extern int gle_cli_buffer_size;
extern int gle_cli_buffer_index;

inline static void multmatrix_d(double m[4][4])
{
	int i, j;
    for(i = 0; i < 4; i++)
       for(j = 0; j < 4; j++)
          gle_cli_matrix[i][j] = m[j][i];
	//MAT_PRINT_4X4(gle_cli_matrix);
}

inline static void v3f(double p[3], int j, int id)
{
	double v4in[4];
	//printf("j=%d id=%d ");
	//VEC_PRINT(p);
   	VEC_COPY(v4in, p);
    v4in[3] = 1;
    //VEC_PRINT_4(v4out);
    if(gle_cli_buffer_index + 3> gle_cli_buffer_size)
    {
        gle_cli_buffer = realloc(gle_cli_buffer, (gle_cli_buffer_size + 256) * sizeof(double));
        gle_cli_buffer_size += 256;
    }
	MAT_DOT_VEC_4X4(gle_cli_buffer + gle_cli_buffer_index, gle_cli_matrix, v4in);
    gle_cli_buffer_index += 3;
}

inline static endtmesh()
{
    fwrite(&gle_cli_buffer_index, sizeof(int), 1, stderr);
    fwrite(gle_cli_buffer, sizeof(double) * gle_cli_buffer_index, 1, stderr);
    fflush(stderr);
    //printf("endtmesh()\n");
    gle_cli_buffer_index = 0;
}

#undef GL_32
#undef OPENGL_10
#define BGNTMESH(i,len)
#define ENDTMESH()      endtmesh();
#define BGNPOLYGON()	printf ("bgnpolygon() \n");
#define ENDPOLYGON()	printf ("endpolygon() \n");
#define V3F_F(x,j,id)	v3f(x, j, id);
#define V3F_D(x,j,id)	v3f(x, j, id);
#define	N3F_F(x)	printf ("n3f(x) 	%f %f %f \n", x[0], x[1], x[2]);
#define	N3F_D(x)	printf ("n3d(x)		%f %f %f \n", x[0], x[1], x[2]);
#define	C3F(x)		printf ("c3f(x) 	%f %f %f \n", x[0], x[1], x[2]);
#define	T2F_F(x)	printf ("t2f(x) 	%f %f \n", x[0], x[1]);
#define	T2F_D(x, y)	printf ("t2d(x)		%f %f \n", x, y);

#define	POPMATRIX()	
#define	PUSHMATRIX()
#define	MULTMATRIX_F(x)	MULTMATRIX_D(x)
#define	LOADMATRIX_F(x)	LOADMATRIX_D(x)
#define LOADMATRIX_D(x)

#define MULTMATRIX_D(x) multmatrix_d(x); (x);
#define __IS_LIGHTING_ON  (0)

