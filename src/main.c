#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gle.h"
double gle_cli_matrix[4][4];
double * gle_cli_buffer = NULL;
int gle_cli_buffer_size = 0, gle_cli_buffer_index = 0;
void dbg(double points[][3], int nbp)
{
	int i;
	for(i = 0; i < nbp; i++)
	{
		double x = points[i][0];
		double y = points[i][1];
		double z = points[i][2];
        printf("%g %g %g\n", x, y, z);
	}
}

int main(int argc, char * argv[])
{
    int ncp, nbpoint, i;
    double (*contour)[2], (*point_array)[3];
    int end = -1;
    double up[3];
    if(argc != 5)
    {
        printf("gle-cli <flag> <Xup> <Yup> <Zup>\n");
        return;
    }

    for(i = 0; i < 3; i++)
        up[i] = atof(argv[2+i]);

    fread(&ncp, sizeof(int), 1, stdin);
    ncp = ncp / 2;
    contour = (double (*) [2])malloc( ncp * sizeof(double) * 2);
    fread(contour, sizeof(double) * 2, ncp, stdin);

    fread(&nbpoint, sizeof(int), 1, stdin);
    nbpoint = nbpoint / 3;
    point_array = (double (*) [3])malloc( nbpoint * sizeof(double[3]));
    fread(point_array, sizeof(double) * 3, nbpoint, stdin);

    //dbg(point_array, nbpoint);
    gleSetJoinStyle (atoi(argv[1]));
    //gleSetJoinStyle (TUBE_JN_RAW);
    gleExtrusion(ncp, contour, NULL, up, nbpoint, point_array, NULL);
    fwrite(&end, sizeof(int), 1, stderr);
}
